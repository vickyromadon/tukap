-- MySQL dump 10.13  Distrib 5.7.23, for Win64 (x86_64)
--
-- Host: localhost    Database: ex_skubidu
-- ------------------------------------------------------
-- Server version	5.7.23-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cabang`
--

DROP TABLE IF EXISTS `cabang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cabang` (
  `uuid_cabang` varchar(36) NOT NULL,
  `nama_cabang` varchar(100) DEFAULT NULL,
  `kode_cabang` varchar(40) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `tgl_create` datetime DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  PRIMARY KEY (`uuid_cabang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cabang`
--

LOCK TABLES `cabang` WRITE;
/*!40000 ALTER TABLE `cabang` DISABLE KEYS */;
/*!40000 ALTER TABLE `cabang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config_server`
--

DROP TABLE IF EXISTS `config_server`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config_server` (
  `ip_server` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config_server`
--

LOCK TABLES `config_server` WRITE;
/*!40000 ALTER TABLE `config_server` DISABLE KEYS */;
/*!40000 ALTER TABLE `config_server` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `denom`
--

DROP TABLE IF EXISTS `denom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `denom` (
  `uuid_denom` varchar(36) NOT NULL,
  `nama_denom` varchar(100) DEFAULT NULL,
  `nominal_denom` decimal(19,0) DEFAULT NULL,
  PRIMARY KEY (`uuid_denom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `denom`
--

LOCK TABLES `denom` WRITE;
/*!40000 ALTER TABLE `denom` DISABLE KEYS */;
INSERT INTO `denom` VALUES ('06ec3126-5333-4ec3-a55f-62921f2633a3','dua puluh ribu',20000),('1b9bb85b-1c20-45d9-8286-c57cf1dd4b3a','seribu',1000),('6dacd85a-ad6b-4c3e-a0c3-eda0d5694429','lima puluh ribu',50000),('7f331eb2-4ae4-413a-974a-d99b68a0b210','seratus ribu',100000),('8f04b4a3-ee47-40f4-bb1f-11bd049d71ff','dua ribu',2000),('8f23837b-9926-4f3d-ab5d-84ad3245cf66','sepuluh ribu',10000),('fed9d053-091c-40de-8c55-8888a884e44d','lima ribu',5000);
/*!40000 ALTER TABLE `denom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `history_harian_saldo`
--

DROP TABLE IF EXISTS `history_harian_saldo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history_harian_saldo` (
  `uuid_history` int(11) NOT NULL,
  `jumlah` decimal(19,0) DEFAULT NULL,
  `tgl_create` datetime DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  PRIMARY KEY (`uuid_history`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `history_harian_saldo`
--

LOCK TABLES `history_harian_saldo` WRITE;
/*!40000 ALTER TABLE `history_harian_saldo` DISABLE KEYS */;
/*!40000 ALTER TABLE `history_harian_saldo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jenis_proses`
--

DROP TABLE IF EXISTS `jenis_proses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jenis_proses` (
  `uuid_menu` varchar(36) NOT NULL,
  `nama_menu` varchar(25) DEFAULT NULL,
  `status_menu` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`uuid_menu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jenis_proses`
--

LOCK TABLES `jenis_proses` WRITE;
/*!40000 ALTER TABLE `jenis_proses` DISABLE KEYS */;
INSERT INTO `jenis_proses` VALUES ('28a8232b-339c-41fe-a6ee-8a4c52dc1800','Penarikan ATM Siang','1'),('295cfec8-de82-4bc3-a803-8338ebd2ee93','Penarikan ATM Pagi','1'),('533880b3-b55d-4f90-b810-3b3ea71e5a5b','Penarikan Cabang','1'),('880f459d-92fb-4717-b817-9d8abcd0391f','Setoran Cabang','1'),('bc39efb7-081b-460e-91a2-3823e6276891','Gagal Replenis ATM','1'),('cd6ecf8c-ff30-4d79-827f-25117b24627b','Cash Return ATM','1');
/*!40000 ALTER TABLE `jenis_proses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proses_trx`
--

DROP TABLE IF EXISTS `proses_trx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proses_trx` (
  `uuid_trx` int(11) NOT NULL,
  `jenis_proses` varchar(45) DEFAULT NULL,
  `peruntukan` varchar(45) DEFAULT NULL,
  `denom_50k` decimal(19,0) DEFAULT NULL,
  `denom_100k` decimal(19,0) DEFAULT NULL,
  `tipe_proses` varchar(36) DEFAULT NULL COMMENT 'Uuid proses',
  `petugas` varchar(36) DEFAULT NULL COMMENT 'uuid petugas',
  `tgl_create` datetime DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  `jenis_transaksi` decimal(19,0) DEFAULT NULL COMMENT 'debit/credit',
  PRIMARY KEY (`uuid_trx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proses_trx`
--

LOCK TABLES `proses_trx` WRITE;
/*!40000 ALTER TABLE `proses_trx` DISABLE KEYS */;
/*!40000 ALTER TABLE `proses_trx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `saldo`
--

DROP TABLE IF EXISTS `saldo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saldo` (
  `uuid_saldo` int(11) NOT NULL,
  `jumlah_saldo` decimal(19,0) DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  PRIMARY KEY (`uuid_saldo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `saldo`
--

LOCK TABLES `saldo` WRITE;
/*!40000 ALTER TABLE `saldo` DISABLE KEYS */;
/*!40000 ALTER TABLE `saldo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipe_porses`
--

DROP TABLE IF EXISTS `tipe_porses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipe_porses` (
  `uuid_tipe` varchar(36) NOT NULL,
  `nama_proses` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`uuid_tipe`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipe_porses`
--

LOCK TABLES `tipe_porses` WRITE;
/*!40000 ALTER TABLE `tipe_porses` DISABLE KEYS */;
INSERT INTO `tipe_porses` VALUES ('40f8ed87-8068-4d3d-a0fb-ccc839b497ba','proyeksi'),('a8169e81-76d2-4328-bfaa-b371e7e5a6ac','realisasi');
/*!40000 ALTER TABLE `tipe_porses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `uuid_user` varchar(36) NOT NULL,
  `nip` varchar(15) NOT NULL,
  `nama_user` varchar(100) DEFAULT NULL,
  `tipe_user_group` varchar(1) DEFAULT NULL COMMENT 'tipe 1 = cabang , 2= vendor',
  `kode_group` varchar(36) DEFAULT NULL COMMENT 'uuid vendor/ cabang',
  `level_user` varchar(1) DEFAULT NULL COMMENT 'User / Administrator',
  `status` varchar(1) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uuid_user`,`nip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('46f72d3a-8ab1-46ae-9291-a083d529fea1','admin','admin','',NULL,'1','1','827ccb0eea8a706c4c34a16891f84e7b');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendor`
--

DROP TABLE IF EXISTS `vendor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendor` (
  `uuid_vendor` varchar(36) NOT NULL,
  `nama_vendor` varchar(100) DEFAULT NULL,
  `kode_vendor` varchar(40) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `tgl_create` datetime DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  PRIMARY KEY (`uuid_vendor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendor`
--

LOCK TABLES `vendor` WRITE;
/*!40000 ALTER TABLE `vendor` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'ex_skubidu'
--

--
-- Dumping routines for database 'ex_skubidu'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-18 18:09:14
