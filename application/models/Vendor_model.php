<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * undocumented class
 */
class Vendor_model extends CI_Model
{
    public function insert($data){
        $INSERT = $this->db->insert('vendor', $data);
        if ($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function edit($id,$data){
        $this->db->where('id_vendor', $id);
        $this->db->update('vendor', $data);
        return true;
    }


    public function allVendor()
    {
        $this->db->select('*');
        $this->db->from('vendor');
        return $this->db->get()->result();
    }

    public function getWhere($array){
        $this->db->select('*');
        $this->db->from('vendor');
        $this->db->where($array);
        return $this->db->get()->row_array();
    }

    
    public function getWhereId($id){
        $this->db->select('nama_vendor');
        $this->db->from('vendor');
        $this->db->where('id_vendor',$id);
        return $this->db->get()->row_array();
	}

	public function getAllVendor()
	{
		$this->datatables->select('uuid_vendor, kode_vendor, nama_vendor, status');
		$this->datatables->from('vendor');
		$this->datatables->add_column('view', '<center>
		<a href="javascript:void(0);" class="editVendor btn btn-info btn-sm" data-uuid_vendor="$1" data-kode_vendor="$2">Edit</a>
		</center>', 'uuid_vendor, kode_vendor');
		return $this->datatables->generate();
	}

	public function getDataEdit($uuid)
	{
		$this->db->select('*');
		$this->db->from('vendor');
		$this->db->where('uuid_vendor', $uuid);
		$query =  $this->db->get();
		return $query->result_array()[0];
	}

	public function getAllDataVendor()
	{
		$this->db->select('*');
		$this->db->from('vendor');
		$query =  $this->db->get();
		return $query->result_array();
	}

	public function update_vendor($data, $uuid)
	{
		$this->db->where('uuid_vendor', $uuid);
		$this->db->update('vendor', $data);
		return true;
	}

	public function checkIsUnique($value, $uuid)
	{
		$this->db->where('uuid_vendor', $uuid);
		$this->db->from('vendor');
		$query  =  $this->db->get();
		$result = $query->result_array()[0];

		if ($result['kode_vendor'] != $value)
			return true;
		else
			return false;
	}


}
/* End of file filename.php */

?>
