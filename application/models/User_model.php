<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{
    public function insert($data){
        $this->db->insert('user', $data);
        if ($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function edit($id,$data){
        $this->db->where('uuid_user', $id);
        $this->db->update('user', $data);
        return true;
    }


    public function allUser()
    {
        $this->db->select('*');
        $this->db->from('user');
        return $this->db->get()->result();
    }

    public function getWhere($array){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where($array);
        return $this->db->get()->row_array();
    }

    
    public function getWhereId($id){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('uuid_user',$id);
        return $this->db->get()->row_array();
	}

	public function getAllUser()
	{
		$this->datatables->select('uuid_user, nip, nama_user, tipe_user_group, level_user, status');
		$this->datatables->from('user');
		$this->datatables->add_column('view', '<center>
		<a href="javascript:void(0);" class="editUser btn btn-info btn-sm" data-uuid_user="$1" data-nip="$2">Edit</a>
		</center>', 'uuid_user, nip');
		return $this->datatables->generate();
	}

	public function getDataEdit($uuid)
	{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('uuid_user', $uuid);
		$query =  $this->db->get();
		return $query->result_array()[0];
	}

	public function getAllDataUser()
	{
		$this->db->select('*');
		$this->db->from('user');
		$query =  $this->db->get();
		return $query->result_array();
	}

	public function update_user($data, $uuid)
	{
		$this->db->where('uuid_user', $uuid);
		$this->db->update('user', $data);
		return true;
	}

	public function checkIsUnique($value, $uuid)
	{
		$this->db->where('uuid_user', $uuid);
		$this->db->from('user');
		$query  =  $this->db->get();
		$result = $query->result_array()[0];

		if ($result['nip'] != $value)
			return true;
		else
			return false;
	}
}
?>
