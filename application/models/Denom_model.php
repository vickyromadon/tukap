<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Denom_model extends CI_Model
{
    public function getDenom(){
        return $this->db->get('denom')->result_array();
    }
}