<div class="col-md-12 mb-4 mt-4" id="edit_user">
	<div class="card">
		<div class="card-header">
			<strong>Edit User</strong>
		</div>

		<form action="" method="post" id="form_edit_user" enctype="multipart/form-data" class="form-horizontal">
			<div class="card-body table-responsive col-md-12">
				<input type="hidden" id="uuid_user" name="uuid_user" value="<?= $data['uuid_user']; ?>" class="form-control">
				<div class="form-group row">
					<label class="col-md-3 col-form-label" for="text-input">NIP</label>
					<div class="col-md-9">
						<input type="text" id="nip" name="nip" value="<?= $data['nip']; ?>" class="form-control">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-3 col-form-label" for="text-input">Nama User</label>
					<div class="col-md-9">
						<input type="text" id="nama_user" name="nama_user" value="<?= $data['nama_user']; ?>" class="form-control">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-3 col-form-label" for="text-input">Status</label>
					<div class="col-md-9">
						<select class="form-control" name="status" id="status">
							<option value='0'>Tidak Aktif</option>
							<option value='1'>Aktif</option>
						</select>
					</div>
				</div>
			</div>

			<div class="card-footer">
				<button type="submit" id="vendor_submit" class="btn btn-sm btn-primary" onclick="edit_user(event)">
					<i class="fa fa-dot-circle-o"></i> Submit
				</button>
				<button type="reset" id="vendor_reset" class="btn btn-sm btn-danger" class="btn btn-secondary" data-dismiss="modal">
					<i class="fa fa-ban"></i> Batal
				</button>
			</div>
		</form>
	</div>
</div>

<script>
	var edit_user = function(event) {
		event.preventDefault();
		var formData = new FormData($('#form_edit_user')[0]);
		$.confirm({
			title: 'Confirm!',
			content: 'Edit data !!',
			buttons: {
				confirm: function() {
					$.ajax({
						url: 'users/update',
						method: 'POST',
						data: formData,
						contentType: false,
						processData: false,
						dataType: "json",
						success: function(data) {
							if (data.status) {
								$.alert(data.message);
								window.location.reload();
							} else {
								$.alert(data.message);
							}
						},
						error: function(error) {
							$.alert(error);
						}
					});
				},
				cancel: function() {},
			}
		});
	}
</script>
