<div class="row">
	<div class="col-md-4 mb-4 mt-4">
		<form action="#" method="post" id="create_user" enctype="multipart/form-data" class="form-horizontal">
			<div class="card">
				<div class="card-header">
					<strong>Form Pendaftaran User</strong>
				</div>
				<div class="card-body">
					<div class="form-group row">
						<label class="col-md-4 col-form-label" for="text-input">NIP</label>
						<div class="col-md-8">
							<input type="text" id="nip" name="nip" class="form-control" placeholder="Masukkan NIP">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-4 col-form-label" for="text-input">Nama User</label>
						<div class="col-md-8">
							<input type="text" id="nama_user" name="nama_user" class="form-control" placeholder="Masukkan Nama User">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-4 col-form-label" for="text-input">Tipe User Group</label>
						<div class="col-md-8">
							<select class="form-control" name="tipe_user_group" id="tipe_user_group">
								<option value=''>-- Pilih Salah Satu --</option>
								<!-- <option value='1'>Cabang</option> -->
								<option value='2'>Vendor</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-4 col-form-label" for="text-input">Kode Group</label>
						<div class="col-md-8">
							<select class="form-control" name="kode_group" id="kode_group">
								<option value=''>-- Pilih Salah Satu --</option>
								<?php
								foreach ($vendors as $item) {
									?>
								<option value="<?= $item->uuid_vendor; ?>"><?= $item->kode_vendor; ?> - <?= $item->nama_vendor; ?></option>
								<?php
								}
								?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-4 col-form-label" for="text-input">Level User</label>
						<div class="col-md-8">
							<select class="form-control" name="level_user" id="level_user">
								<option value=''>-- Pilih Salah Satu --</option>
								<option value='1'>User</option>
								<option value='2'>Administrator</option>
							</select>
						</div>
					</div>
				</div>
				<div class="card-footer">
					<button type="submit" id="user_submit" class="btn btn-sm btn-primary" onclick="submitFormUser(event)"><i class="fa fa-dot-circle-o"></i> Submit</button>
					<button type="reset" id="user_reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i> Reset</button>
				</div>
			</div>
		</form>
	</div>
	<div class="col-md-8 mb-4 mt-4">
		<div class="card">
			<div class="card-header">
				<strong>Daftar User</strong>
			</div>
			<div class="card-body table-responsive">
				<table id="table_user" class="table-striped table-sm table table-bordered table-condensed table-hover " cellspacing="0" width="tabelListDbs%">
					<thead style="background-color:#bfe7bf">
						<tr>
							<th>NIP</th>
							<th>Nama</th>
							<th>Tipe Group</th>
							<th>Level</th>
							<th>Status</th>
							<th>Opsi</th>
						</tr>
					</thead>
				</table>
			</div>
			<div class="card-footer">
			</div>
		</div>
	</div>
</div>

<!-- The Modal -->
<div class="modal" id="myModal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" id="bg-modal">
			<!-- Modal body -->
			<div class="modal-body">
				<div id="result_data"></div>
			</div>
			<!-- Modal footer -->
		</div>
	</div>
</div>

<script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>

<script>
	function submitFormUser(event) {
		event.preventDefault();
		var nip = document.getElementById("nip").value;
		var nama_user = document.getElementById("nama_user").value;
		var tipe_user_group = document.getElementById("tipe_user_group").value;
		var level_user = document.getElementById("level_user").value;
		var kode_group = document.getElementById("kode_group").value;

		var formData = new FormData();

		formData.append('nip', nip);
		formData.append('nama_user', nama_user);
		formData.append('tipe_user_group', tipe_user_group);
		formData.append('level_user', level_user);
		formData.append('kode_group', kode_group);

		$.confirm({
			title: 'Form User',
			content: 'Submit User ?',
			buttons: {
				confirm: function() {
					$.ajax({
						url: "users/create",
						type: 'POST',
						data: formData,
						cache: false,
						contentType: false,
						processData: false,
						dataType: "json",
						success: function(data) {
							if (data.status) {
								$.alert(data.message);
								window.location.reload();
							} else {
								$.alert(data.message);
							}
						},
						error: function(error) {
							$.alert(error);
						}
					});
				},
				cancel: function() {}
			}
		});
	}

	$(document).ready(function() {
		$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
			return {
				"iStart": oSettings._iDisplayStart,
				"iEnd": oSettings.fnDisplayEnd(),
				"iLength": oSettings._iDisplayLength,
				"iTotal": oSettings.fnRecordsTotal(),
				"iFilteredTotal": oSettings.fnRecordsDisplay(),
				"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
				"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
			};
		};

		var tableVendor = $("#table_user").DataTable({
			"dom": 'Zlfrtip',
			initComplete: function() {
				var api = this.api();
				$('#table_user_filter input')
					.off('.DT')
					.on('input.DT', function() {
						api.search(this.value).draw();
					});
			},
			oLanguage: {
				"sUrl": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Indonesian.json",
				sProcessing: "loading..."
			},
			processing: true,
			serverSide: true,
			ajax: {
				"url": "users/getUserJson",
				"type": "POST"
			},
			columns: [{
					"data": "nip"
				},
				{
					"data": "nama_user"
				},
				{
					"data": "tipe_user_group",
					"render": function(data, type, row, meta) {
						if (data == '1') {
							return 'Cabang';
						} else {
							return 'Vendor';
						}
					}
				},
				{
					"data": "level_user",
					"render": function(data, type, row, meta) {
						if (data == '1') {
							return 'User';
						} else {
							return 'Administrator';
						}
					}
				},
				{
					"data": "status",
					"render": function(data, type, row, meta) {
						if (data == '1') {
							return 'Aktif';
						} else {
							return 'Tidak Aktif';
						}
					}
				},
				{
					"data": "view"
				}
			],
			order: [
				[0, 'asc']
			],
			rowCallback: function(row, data, iDisplayIndex) {
				var info = this.fnPagingInfo();
				var page = info.iPage;
				var length = info.iLength;
				$('td:eq(0)', row).html();
			}
		});

		$('#table_user').on('click', '.editUser', function() {
			var uuid = $(this).data('uuid_user');
			console.log(uuid);
			$('#myModal').modal('show')
			$.ajax({
				type: 'get',
				url: 'users/edit?uuid=' + uuid,
				success: function(result) {
					$('#result_data').html(result);
				}
			});
		});
	});
</script>
