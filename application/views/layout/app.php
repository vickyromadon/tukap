<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?php echo $title; ?></title>
	<link href="<?php echo base_url() ?>assets/css/timepicker.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap-table.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap-table-fixed-columns-pro.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css">
	<link href="<?php echo base_url() ?>assets/css/gijgo.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
	<link href="<?php echo base_url('assets/css/dataTables.bootstrap.css') ?>" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/fixedcolumns/3.2.5/css/fixedColumns.bootstrap4.min.css">
	<link rel="stylesheet" href="//cdn.datatables.net/fixedcolumns/3.2.6/css/fixedColumns.dataTables.min.css">
	<link rel="stylesheet" href="//cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
	<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/scroller/1.5.1/css/scroller.dataTables.min.css">


</head>

<body>
	<div class="container-fluid">
		<?php $this->load->view('layout/menu'); //Loading Top Main Menu Navigation 
		?>
		<?php $this->load->view($contents); ?>
	</div>
	<div class="modal fade" role="document" id="basicModal" tabindex="-1" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Modal title</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Modal body text goes here.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary">Save changes</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/timepicker.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/bootstrap-table.js"></script>
	<script src="<?php echo base_url() ?>assets/js/bootstrap-table-fixed-columns-pro.js"></script>
	<script src="<?php echo base_url() ?>assets/js/gijgo.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
	<script src="<?php echo base_url('assets/js/jquery.dataTables.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/dataTables.bootstrap.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/dataTables.colResize.js') ?>"></script>
	<script src="//cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js" charset="utf-8"></script>
	<script src="//cdn.datatables.net/scroller/1.5.1/js/dataTables.scroller.min.js" charset="utf-8"></script>

	<script>
		$('#tgl_claim').datepicker({
			// "setDate": new Date(),
			format: 'dd/mm/yyyy',
			uiLibrary: 'bootstrap4'
		})

		$('#tgl_input').datepicker({
			format: 'dd/mm/yyyy',
			uiLibrary: 'bootstrap4'
		});

		$('#tgl_terima_uang').datepicker({
			format: 'dd/mm/yyyy',
			uiLibrary: 'bootstrap4'
		});

		$('#tgl_email').datepicker({
			format: 'dd/mm/yyyy',
			uiLibrary: 'bootstrap4'
		});
		$('#tgl_tindakan').datepicker({
			format: 'dd/mm/yyyy',
			uiLibrary: 'bootstrap4'
		});

		$('#tgl_claim_filer').datepicker({
			format: 'dd/mm/yyyy',
			uiLibrary: 'bootstrap4'
		});
		$('#tgl_terima_uang_filter').datepicker({
			format: 'dd/mm/yyyy',
			uiLibrary: 'bootstrap4'
		});
		$('#tgl_email_filter').datepicker({
			format: 'dd/mm/yyyy',
			uiLibrary: 'bootstrap4'
		});
		$('#tgl_input_filter').datepicker({
			format: 'dd/mm/yyyy',
			uiLibrary: 'bootstrap4'
		});
	</script>
	<script>
		$('#demoTable').bootstrapTable({
			search: true,
			pagination: true,
			toolbar: '.toolbar',
			fixedColumns: true,
			fixedFrom: 'left',
			fixedNumber: 1
		});
		$('a[href$="#basicModal"]').on("click", function() {
			$('#basicModal').modal('show');
		});
	</script>
	<script src="<?php echo base_url() ?>assets/js/js.js" type="text/javascript"></script>
</body>

</html>
