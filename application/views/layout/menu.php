<div class="row mt-2">
    <!-- navigation -->
    <div class="col-md-1">
        <img src="<?php echo base_url() ?>assets/img/logo.png" alt="" style="margin-top:-19px">
    </div>
    <div class="col-md-8">
        <ul class="nav nav-pills">
            <li class="nav-item">
                <a class="nav-link <?php if($title=='Cabang') { echo 'active'; } ?>"  
                   href="<?php echo site_url('app/cabang') ?>">Cabang</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php if($title=='Denom') { echo 'active'; } ?>" href="<?php echo site_url('app/denom') ?>"> Denom</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php if($title=='Vendor') { echo 'active'; } ?>" href="<?php echo site_url('vendors') ?>">Vendor</a>
            </li>
            <?php
               // if($admin['level']=='superadmin'){
            ?>
                <li class="nav-item">
                    <a class="nav-link <?php if($title=='User') { echo 'active'; } ?>" href="<?php echo site_url('users') ?>">User</a>
                </li>
            <?php
              //  }
            ?>
        </ul>
    </div>
    <div class="col-md-3">
        <div class="float-right">
          <label class="label mt-2 mr-2" style="font-weight:600;"><?php// echo strtoupper($admin['nama_petugas']).' | '.strtoupper($admin['level']);?> </label>
          <a href="<?php echo base_url()?>logout" type="button" class="btn btn-gray">Logout</a>
        </div>
    </div>
</div>
