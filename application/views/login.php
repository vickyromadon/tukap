<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Login Page</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
</head>
<body>
    <div class="container-fluid">
        <div class="col-md-4 offset-md-4 mt-5">
        <div class="mt-4"></div>
       
        <div class="alert alert-danger print-error-msg" style="display:none"></div>
            <form id="login">
                <fieldset>
                    <legend class="text-center">LOGIN</legend>
                    <?php
                        if($this->session->flashdata('need_login')):
                            echo'<div class="alert alert-danger print-error-msg">';
                            echo $this->session->flashdata('need_login');
                            echo "</div>";
                        endif;
                    ?>
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" class="form-control" name="username" id="username" aria-describedby="cabangHelp" placeholder="Enter Username" >
                            <div id="usernameHelp"></div>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" id="password" aria-describedby="password" placeholder="Enter Password">
                            <div id="passwordHelp"></div>
                        </div>
                        <button type="submit" class="col-md-12 btn btn-login  btn-primary">Login</button>
                </fieldset>
            </form>
        </div>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/js.js" type="text/javascript"></script>
</body>
</html>