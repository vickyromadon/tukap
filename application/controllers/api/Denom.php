<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Denom extends CI_Controller {

    use REST_Controller { REST_Controller::__construct as private __resTraitConstruct; }
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->__resTraitConstruct();
        $this->load->model('denom_model','denom');
    }

    public function denoms_get()
    {
        $denom=$this->denom->getDenom();
        if($denom){
            $this->response($denom, 200); // 
        }
    }
}
