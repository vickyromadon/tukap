<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// End load library phpspreadsheet
class Dashboard extends CI_Controller {
	public function __construct() {
		parent::__construct(); 
	

		// if($this->session->userdata('haiLogin') == FALSE)
        // {
        //     $this->session->set_flashdata('need_login','Anda harus login terlebih dahulu.');
        //     redirect('login','refresh');
        // }

		$this->load->model('jenis_proses_model','jenis_proses');


	}


	public function index()
	{
		
		// $tipe_user=$this->session->userdata('level');

		// $data['admin']=$this->session->userdata;

		$data['title']         = 'Dashboard';
		$data['contents']      = 'dashboard';
		// $data['vendor']        = $this->vendor_model->allVendor();
		// $data['cabang']        = $this->cabang_model->allCabang();
		// $data['case']          = $this->cases_model->allCase();
		// $data['denom']         = $this->denom_model->allDenom();
		// $data['tindak_lanjut'] = $this->tindakan_model->allTindakan();

	    // $id_petugas            = $this->session->userdata('id_petugas');
		// $data_rekaps           = $this->rekap_model->allRekap($id_petugas);

		// foreach ($data_rekaps as $data_rek) {

			
		// 	$data_rek->case  =$this->cases_model->getWhereId($data_rek->case)['nama_case'];
		// 	$data_rek->denom =$this->denom_model->getWhereId($data_rek->denom)['nama_denom'];
		// 	$data_rek->vendor=$this->vendor_model->getWhereId($data_rek->vendor)['nama_vendor'];
		// 	$data_rek->cabang=$this->cabang_model->getWhereId($data_rek->cabang)['nama_cabang'];
		// 	$data_rek->petugas=$this->petugas_model->getWhereId($data_rek->petugas)['nama_petugas'];
		// 	$data_rek->tindak_lanjut=$this->tindakan_model->getWhereId($data_rek->tindak_lanjut)['nama_tindakan'];
		// }
		// // print_r($id_petugas);
		// $data['rekaps']=$data_rekaps;


		// // ! Set edit data
		// // * Get Id Rekap
		// $rekap_id=$this->input->get('rekap_id');
        // if(isset($rekap_id)){
        //     $data['rekap']        =  $this->rekap_model->_get_where(array('id_rekap'=>$rekap_id))->row_array();
		// }
		$data['kategori_proses']         = $this->jenis_proses->allJenisProses();
		$this->load->view('layout/app',$data);
	}
}
