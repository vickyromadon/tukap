<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('user_model', 'admin');
    }

    public function index()
    {
        $this->load->view('login');
    }

    public function login_user()
    {
        $jsonresult = array();
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('passuser', 'Password', 'required');
        // $this->form_validation->set_rules('tipe', 'Tipe User', 'callback_custom_dropdown');

        if ($this->form_validation->run() == false) {
            // $errors = validation_errors();
            $jsonresult['status_valid'] = 'false';
            $jsonresult['err_username'] = form_error('username', '<small class="id-help form-text text-danger">', '</small>');
            $jsonresult['err_password'] = form_error('passuser', '<small class="id-help form-text text-danger">', '</small>');
        } else {
            $jsonresult['status_valid'] = 'true';

            $username = $this->input->post('username');
            $password = $this->input->post('passuser');

            $data_user = array(
                'nip' => $username,
                'password' => md5($password),
            );

            $check_exist_user = $this->admin->getWhere($data_user);
            if (empty($check_exist_user)) {
                $jsonresult['login'] = 'false';
                $jsonresult['login_info'] = 'Login Gagal / Username Password Salah';
            } else {
                // set session user kecuali password
                // direct to dashboad
                // unset($check_exist_user['password']);
                // $check_exist_user['haiLogin'] = 'login';
                // $this->session->set_userdata($check_exist_user);

                // update data
                $jsonresult['login'] = 'true';
                $jsonresult['url'] = base_url() . 'dashboard';
            }
        }
        echo json_encode($jsonresult);
    }
}
