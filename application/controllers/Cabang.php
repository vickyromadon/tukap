<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cabang extends CI_Controller {
   
    public function __construct() {
	parent::__construct(); 
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('cabang_model','cabang');
 	}

	public function index()
	{
		$data['admin']=$this->session->userdata;

		$cabang=$this->input->get('cabang_id');
		if(isset($cabang)||$cabang!=null){
			$data['detail_cabang']=$this->cabang_model->getWhere(array('id_cabang'=>$cabang));
		}

		$data['title']    = 'Cabang';
		$data['tipe']     = 'Index';
		$data['contents'] = 'v_cabang';
		$data['cabangs']   = $this->cabang_model->allCabang();
		$this->load->view('layout/app', $data);
	}
	
	public function edit(){
		$this->form_validation->set_rules('idCabang', 'Id Cabang', 'required');
		$this->form_validation->set_rules('kodeCabang', 'Kode Cabang', 'required');
        $this->form_validation->set_rules('namaCabang', 'Nama Cabang', 'required');

        if ($this->form_validation->run() == FALSE){
            $errors = validation_errors();
            echo json_encode(['error'=>$errors]);
        }else{

			$idCabang    = $this->input->post('idCabang');
			$kodeCabang  = $this->input->post('kodeCabang');
			$namaCabang  = $this->input->post('namaCabang');

			$data_cabang=array(
				'kode_cabang'              => $kodeCabang,
				'nama_cabang'              => $namaCabang,
			);

			$update=$this->cabang_model->edit($idCabang,$data_cabang);
			if($update){
				echo json_encode(['success'=>'Cabang Update successfully.']);
			}else{
				echo json_encode(['success'=>'Update Gagal.']);
			}
        }
	}

	public function add(){
        $this->form_validation->set_rules('kodeCabang', 'Kode Cabang', 'required');
        $this->form_validation->set_rules('namaCabang', 'Nama Cabang', 'required');

        if ($this->form_validation->run() == FALSE){
            $errors = validation_errors();
            echo json_encode(['error'=>$errors]);
        }else{

			$kodeCabang  = $this->input->post('kodeCabang');
			$namaCabang  = $this->input->post('namaCabang');

			$data_cabang=array(
				'kode_cabang'              => $kodeCabang,
				'nama_cabang'              => $namaCabang,
			);

			$check_exist_kode_cabang=$this->cabang_model->getWhere(array('kode_cabang'=>$kodeCabang));
			if(empty($check_exist_kode_cabang)){
				$add_cabang=$this->cabang_model->insert($data_cabang);
				echo json_encode(['success'=>'Cabang added successfully.']);
			}else{
				echo json_encode(['success'=>'Kode Cabang Sudah Terdaftar.']);
			}
        }
	}
    
    public function non_aktif(){
        
    }
}